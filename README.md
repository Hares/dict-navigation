# Python Dict Navigation

A light-weight library for simple navigation in dict like objects.

##### Example:
```code=python
from dict_navigation import get_element_in_dict_object, set_element_in_dict_object
import json

_dict = { 'foo': {'bar': 3.14, 'car': 2.7, 'dar': 0}, 'boo': 2 }
print("Foo's Bar is: {}".format(get_element_in_dict_object(_dict, 'foo/bar')))
set_element_in_dict_object(_dict, 'foo/dar', 15)
set_element_in_dict_object(_dict, 'foo/var/tzar', 'Ivan the Terrible', recursive=True )
print(json.dumps(_dict, sort_keys=True))
```

This will print:
```
Foo's Bar is: 3.14
{"boo": 2, "foo": {"bar": 3.14, "car": 2.7, "dar": 15, "var": {"tzar": "Ivan the Terrible"}}}
```
